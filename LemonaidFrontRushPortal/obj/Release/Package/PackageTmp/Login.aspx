﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="LemonaidFrontRushPortal.Login" EnableEventValidation="false" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1" />
    <title>FrontRush Login</title>
    <link rel="icon" href="img/favicon.png" type="image/png" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/linecons.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:400,900,700,700italic,400italic,300italic,300,100italic,100,900italic' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,500,700,800,600,300,200' rel='stylesheet' type='text/css' />
    <script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/classie.js"></script>
</head>
<body>
    <form id="form2" runat="server">
        <div class="container">
            <div class="athletics-page">
                <!--Header_section-->
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="LemonaidAdmin.aspx">
                                <img src="img/logo.png" /></a>
                        </div>
                        <ul class="nav navbar-nav">
                        </ul>
                    </div>
                </nav>
                <!--Header_section-->
                <!--Top_content-->
                <section id="top_content" class="top_cont_outer">
                    <div class="top_cont_inner">
                        <div class="gradient"></div>
                        <div class="container">
                            <div class="top_content">
                                <div class="top_left_cont delay-03s animated wow zoomIn">
                                    <h2>FRONT RUSH</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="service">
                    <div class="service_area">
                        <div class="row">
                            <div class="col-lg-12 col-xs-12">
                                <div class="form-wrap">
                                    <div class="tabs">
                                        <h3 class="login-tab"><a class="active" href="#login-tab-content">Login</a></h3>

                                    </div>
                                    <div class="tabs-content">
                                        <div id="login-tab-content" class="active">
                                            <form class="login-form" action="" method="post">
                                                <div class="form-group">

                                                    <div class="form-group">

                                                        <asp:TextBox ID="txtUserName" placeholder="UserName" CssClass="form-control" runat="server" />
                                                        <asp:RequiredFieldValidator ID="rfvUser" ErrorMessage="Please enter Username" ControlToValidate="txtUserName" runat="server" />
                                                    </div>
                                                    <div class="form-group">

                                                        <asp:TextBox ID="txtPWD" runat="server" placeholder="Password" CssClass="form-control" TextMode="Password" />
                                                        <asp:RequiredFieldValidator ID="rfvPWD" runat="server" ControlToValidate="txtPWD" ErrorMessage="Please enter Password" />
                                                    </div>
                                                    <div class="form-group"> 
                                                        <asp:Button ID="btnSubmit" runat="server" Text="Login" class="btn btn-danger" OnClick="btnSubmit_Click" />
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label ID="labelDisplayStatus" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                                                        <br />
                                                    </div>
                                                </div>
                                            </form>
                                              <div class="help-text">
                                                <p><a href="#">Forget your password?</a></p>
                                            </div>
                                            <div class="help-text">
                                                <p>New User ? <a href="SignUp.aspx">SIGN UP</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Service-->

                <!--main-section-end-->
                <!--twitter-feed-end-->
                <footer class="footer_section" id="contact">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12">
                            <div class="footer_bottom"><span>© 2016 Athletics training. All Rights Reserved.</span> </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </form>
</body>
</html>

