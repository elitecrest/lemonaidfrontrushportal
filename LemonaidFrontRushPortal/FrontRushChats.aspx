﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrontRushChats.aspx.cs" Inherits="LemonaidFrontRushPortal.first" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1" />
    <title>Front Rush</title>
    <link rel="icon" href="img/favicon.png" type="image/png" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/linecons.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:400,900,700,700italic,400italic,300italic,300,100italic,100,900italic' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,500,700,800,600,300,200' rel='stylesheet' type='text/css' />
    <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->
    <script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/classie.js"></script>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="Scripts/ScrollableGridPlugin.js" type="text/javascript"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/Scripts/jquery-1.8.2.min.js"></script>

    <script src="/Scripts/ui/jquery.ui.core.js"></script>
    <script src="/Scripts/ui/jquery.ui.widget.js"></script>
    <script src="/Scripts/ui/jquery.ui.mouse.js"></script>
    <script src="/Scripts/ui/jquery.ui.draggable.js"></script>
    <script src="/Scripts/ui/jquery.ui.resizable.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".messageArea").animate({ scrollTop: $(document).height() }, 1);
            return false;
        });
    </script>

    <style type="text/css">
        .Initial {
            display: block;
            padding: 4px 18px 4px 18px;
            float: left;
            background: url('../Images/InitialImage.png') !important no-repeat right top;
            color: Black;
            font-weight: bold;
        }

            .Initial:hover {
                color: White;
                background: url('../Images/SelectedButton.png') !important no-repeat right top;
            }

        .Clicked {
            float: left;
            display: block;
            background: url("../Images/SelectedButton.png") !important no-repeat right top;
            padding: 4px 18px 4px 18px;
            color: Black;
            font-weight: bold;
            color: White;
        }

        .FixedHeader {
            position: absolute;
            font-weight: bold;
        }

        .mydatagrid {
            width: 50%;
            border: solid 2px black;
            min-width: 60%;
        }

        .header {
            background-color: #000;
            font-family: Arial;
            color: White;
            height: 25px;
            text-align: center;
            font-size: 16px;
        }

        .rows {
            background-color: #fff;
            font-family: Arial;
            font-size: 18px;
            color: #000;
            min-height: 25px;
            text-align: center;
        }

        /*.rows:hover {
                background-color: #22c81a;
            }*/

        .mydatagrid a /** FOR THE PAGING ICONS  **/ {
            background-color: Transparent;
            padding: 5px 5px 5px 5px;
            color: #000;
            text-decoration: none;
            font-weight: bold;
        }

            .mydatagrid a:hover /** FOR THE PAGING ICONS  HOVER STYLES**/ {
                background-color: #fff;
                color: #000;
            }

        /*.mydatagrid span {
            padding: 5px 5px 5px 5px;
            background-color: #000;
            color: #fff;
        }*/

        .pager {
            background-color: #22c81a;
            font-family: Arial;
            color: White;
            height: 30px;
            text-align: left;
        }

        .mydatagrid td {
            padding: 2px;
        }

        .mydatagrid th {
            padding: 0px;
        }

        .chatbox {
            position: fixed;
            background: #808080;
            width: 250px;
            height: 300px;
            z-index: 10;
            border-color: black;
            column-width: auto;
            word-spacing: normal;
            clear: both;
            top: 264px;
            left: 10px;
        }

        body {
            padding: 0px;
            margin: 0px;
        }

        #header {
            padding: 20px;
            background-color: #d7e5e4;
            border-bottom: #5f9482 solid 2px;
            color: #1e4638;
            font-size: 24px;
        }

        .submitButton {
            background-color: #d7e5e4;
            border: #5f9482 solid 1px;
            padding: 4px;
            cursor: pointer;
        }

            .submitButton:hover {
                background-color: #e1e1e1;
            }

        .login {
            width: 250px;
            margin-left: auto;
            margin-right: auto;
            padding: 10px;
            border: solid 2px #5f9482;
            color: #1e4638;
        }

            .login .textBox {
                width: 240px;
            }

            .login #divButton {
                padding: 4px;
                text-align: right;
            }

        .chatRoom {
            width: 650px;
            margin-left: auto;
            margin-right: auto;
            border: solid 1px gray;
        }

            .chatRoom .title {
                font-size: 16px;
                font-weight: bold;
                padding: 8px;
                background-color: #d7e5e4;
                border-bottom: solid 1px #5f9482;
            }

            .chatRoom .content {
                height: 300px;
                clear: both;
            }

                .chatRoom .content .chatWindow {
                    float: left;
                    width: 409px;
                    height: 300px;
                    border-right: solid 1px #5f9482;
                    overflow-y: scroll;
                }

                    .chatRoom .content .chatWindow .message {
                        padding: 4px;
                    }

                        .chatRoom .content .chatWindow .message .userName {
                            font-weight: bold;
                        }

                .chatRoom .content .users {
                    float: right;
                    width: 240px;
                    height: 300px;
                }

                    .chatRoom .content .users .user {
                        display: block;
                        cursor: pointer;
                        padding: 4px;
                        background-color: #f9f9f9;
                        border-bottom: solid 1px #5f9482;
                    }

                    .chatRoom .content .users .loginUser {
                        display: block;
                        padding: 4px;
                        color: gray;
                        border-bottom: solid 1px #5f9482;
                    }

                    .chatRoom .content .users .user:hover {
                        background-color: #e1e1e1;
                    }

            .chatRoom .messageBar {
                border-top: solid 1px gray;
                padding: 4px;
            }

                .chatRoom .messageBar .textbox {
                    width: 550px;
                }

        .disconnect {
            position: absolute;
            margin: 10px;
            background-color: #ffcbcb;
            padding: 4px;
            border: solid 1px red;
        }


        .draggable {
            position: absolute;
            border: #5f9482 solid 1px !important;
            width: 350px;
        }

            .draggable .header {
                cursor: move;
                background-color: #d7e5e4;
                border-bottom: #5f9482 solid 1px;
                color: #1e4638;
            }

            .draggable .selText {
                vertical-align: middle;
                color: black;
                padding: 4px;
            }

            .draggable .messageArea {
                width: 347px;
                overflow-y: scroll;
                height: 280px;
                border-bottom: #5f9482 solid 1px;
                padding: 4px;
            }

                .draggable .messageArea .message {
                    padding: 4px;
                }

            .draggable .buttonBar {
                width: 350px;
                padding: 4px;
            }

                .draggable .buttonBar .msgText {
                    width: 270px;
                }

                .draggable .buttonBar .button {
                    margin-left: 4px;
                    width: 55px;
                }

        .logout {
            float: right;
        }

        .GridHeader {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form2" runat="server">
        <div class="container">
            <div class="athletics-page">
                <!--Header_section-->
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <%--<a href="#">content</a>>--%>
                                <img src="img/logo.png" /></a>
                        </div>
                        <div style="text-align: right">
                            <%--    <table>
                                <tr>
                                    <td style="text-align: right">--%>
                            <a href="Login.aspx">Logout</a>

                            <%--  </td>
                                </tr>
                            </table>--%>
                        </div>
                    </div>
                </nav>
                <!--Header_section-->
                <!--Top_content-->
                <section id="top_content" class="inner_banner">
                    <div class="inner_banner_cont">
                        <div class="gradient"></div>
                        <div class="container">
                            <div class="inner_banner_ttl">
                                <div class="inner_banner_left_ttl delay-03s animated wow fadeInLeft">
                                    <h2>Athlete Front Rush Chats</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="service">
                    <div class="service_area">
                        <div class="row">
                            <table width="99%" align="center">
                                <tr>
                                    <td>

                                        <table style="width: 100%; border-width: 0px; border-color: #666; border-style: solid">
                                            <tr>
                                                <td>
                                                    <asp:GridView runat="server" ID="datagrid" Width="80%" AutoGenerateColumns="false"
                                                        OnRowDataBound="datagrid_RowDataBound" OnRowCommand="datagrid_RowCommand">
                                                        <RowStyle BackColor="#EFF3FB" />

                                                        <HeaderStyle BackColor="#204eaa" Font-Bold="True" ForeColor="White" />
                                                        <AlternatingRowStyle BackColor="linen" />
                                                        <Columns>
                                                            <asp:TemplateField Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAtheleteId" runat="server" Text='<%# Eval("AthleteId") %>'></asp:Label>
                                                                    <asp:Label ID="lblLoginType" runat="server" Text='<%# Eval("LoginType") %>'></asp:Label>
                                                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Emailaddress") %>'></asp:Label>
                                                                    <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("Username") %>'></asp:Label>
                                                                    <asp:Label ID="lblSportId" runat="server" Text='<%# Eval("SportId") %>'></asp:Label>
                                                                     <asp:Label ID="lblAdId" runat="server" Text='<%# Eval("AdId") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:ImageField DataImageUrlField="AthleteProfilePic" ControlStyle-Width="100" ItemStyle-HorizontalAlign="Center"
                                                                ControlStyle-Height="100" HeaderText="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Athlete ProfilePic" />
                                                            <asp:TemplateField HeaderText="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Athlete Name" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAtheleteName" Text='<%# Eval("AthleteName") %>' runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Messages" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcount" Text='<%# Eval("ChatCount") %>' runat="server"> </asp:Label>
                                                                    <%--   <div class="glyphicon">
                                                    <i class="glyphicon glyphicon-envelope form-control-feedback"></i>--%>

                                                                    <%--    <asp:Button ID="Button1" runat="server" BackColor="Transparent" BorderWidth="0" CommandName="OpenChat"
                                                        CommandArgument='<%# Eval("ChatCount") %>' CssClass="btn btn-default glyphicon glyphicon-envelope btn-lg" />--%>
                                                                    <asp:ImageButton ID="Button1" runat="server" BackColor="Transparent" BorderWidth="0" CommandName="OpenChat"
                                                                        CommandArgument='<%# Eval("ChatCount") %>' ImageUrl="~/Images/disableChat.png" Width="25" Height="25" />
                                                                    <%--  </div>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeviceId" runat="server" Text='<%# Eval("DeviceId") %>'></asp:Label>
                                                                    <asp:Label ID="lblDeviceType" runat="server" Text='<%# Eval("DeviceType") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>

                                    <td style="width: 40%; text-align: center; vertical-align: top">
                                        <div id="ChatWindow" class="ui-widget-content draggable" visible="false" rel="0" runat="server">
                                            <div class="header">
                                                <div style="float: right;">
                                                    <%--<img id="imgDelete" style="cursor: pointer;" src="/Images/delete.png" onclick="closeIt();"/>--%>
                                                    <%--  <asp:Image ID="CloseWindow" runat="server" src="/Images/delete.png" />--%>
                                                    <asp:ImageButton ID="imagebutton1" runat="server" ImageUrl="/Images/delete.png" OnClick="imagebutton1_Click" />
                                                </div>

                                                <%--  <span class="selText" rel="0">' + userName + '</span>--%>
                                                <asp:Label ID="lblAtheleteName" runat="server" CssClass="selText"></asp:Label>
                                            </div>
                                            <div id="divMessage" class="messageArea" runat="server">
                                                <%--    <asp:ListBox ID="AllMsgs" runat="server" Height="225" Width="245"></asp:ListBox>--%>
                                                <table id="ChatData" runat="server" style="width: 100%;">
                                                </table>
                                            </div>
                                            <div class="buttonBar">
                                                <%--<input id="txtPrivateMessage" class="msgText" type="text" />--%>
                                                <asp:TextBox ID="txtPrivateMessage" CssClass="msgText" runat="server" onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                <%-- <input id="btnSendMessage" class="submitButton button" type="button" value="Send" />--%>
                                                

                                                 <asp:Label ID="lblAdIdSend" runat="server" Visible="false"></asp:Label>
                                                 <asp:Label ID="lblSportIdSend" runat="server" Visible="false"></asp:Label>
                                                 <asp:Label ID="lblloginTypeSend" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblEmailUserNameSend" runat="server" Visible="false"></asp:Label>
                                                

                                                <asp:Label ID="lblAthIdSend" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAthDevId" runat="server" Visible="false"></asp:Label>
                                                <asp:Button ID="btnSendMessage" CssClass="submitButton button" Text="Send" runat="server" OnClick="btnSendMessage_Click" OnClientClick="gotoBottom()" />
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </section>
                <!--Service-->
                <!--twitter-feed-end-->
                <footer class="footer_section" id="contact">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12">
                            <div class="footer_bottom"><span>© 2016 Front Rush. All Rights Reserved.</span> </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </form>
</body>
</html>
