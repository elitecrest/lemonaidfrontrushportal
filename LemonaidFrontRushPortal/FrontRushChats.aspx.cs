﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Drawing;

namespace LemonaidFrontRushPortal
{
    public partial class first : System.Web.UI.Page
    {
        public static string baseurl = ConfigurationManager.AppSettings["BaseURl"].ToString();

        //public static SqlConnection ConnectTodb()
        //{
        //    string connectionString = ConfigurationManager.ConnectionStrings["LemonaidDB"].ConnectionString;
        //    SqlConnection sqlconn = new SqlConnection(connectionString);
        //    sqlconn.Open();
        //    return sqlconn;
        //}
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("hi-IN");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("hi-IN");
                Deserialize_json_Object_and_Bind_GridView();
            }
        }
        public void Deserialize_json_Object_and_Bind_GridView()
        {

            int EndAdUserId = (int)System.Web.HttpContext.Current.Session["userId"];

            string url = "Ad/GetAthletesForAd?EndAdUserId=" + EndAdUserId + "&AdType=4";

            //WebRequest req = WebRequest.Create(url);
            //req.ContentType = "application/json";
            //WebResponse resp = req.GetResponse();
            //Stream stream = resp.GetResponseStream();
            //StreamReader re = new StreamReader(stream);
            //String json = re.ReadToEnd();
            // json = "{\"Chats\":" + json + "}";
            ////string subJson = json.Substring(23, json.Length - 64);


            // DataTable dt = JsonStringToDataTable(json);

            CustomResponse result = APICalls.Get("Ad/GetAthletesForAd?EndAdUserId=" + EndAdUserId + "&AdType=4");
            if (result.Response != null)
            {
                JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                serializer2.MaxJsonLength = 1000000000;
                var Athletes = result.Response.ToString();

                List<AthleteandAdDTO> AthletsData = serializer2.Deserialize<List<AthleteandAdDTO>>(Athletes);
                //var AthletsJson = new JavaScriptSerializer().Serialize(AthletsData);

                //DataTable AthleteDataTable = JsonStringToDataTable(AthletsJson);
                ////SportsTable.Rows.Add("Select Sport", "Select Sport");
                //DataView view = AthleteDataTable.DefaultView;
                //view.Sort = "Id";
                datagrid.DataSource = AthletsData;
                datagrid.DataBind();

            }
            else
            {
                lblMsg.Text = "No Athelete Chats Found!!!!";
                lblMsg.ForeColor = System.Drawing.Color.Red;

            }
            // if (dt.Rows[0]["Chats"].ToString().Contains("0"))
            //{
            //    datagrid.DataSource = dt;
            //    datagrid.DataBind();
            //}
            //else
            //{
            //    lblMsg.Text = "No Athelete Chats Found!!!!";
            //    lblMsg.ForeColor = System.Drawing.Color.Red;
            //}

        }

        public DataTable JsonStringToDataTable(string jsonString)
        {
            DataTable dt = new DataTable();
            string[] jsonStringArray = Regex.Split(jsonString.Replace("[", "").Replace("]", ""), "},{");
            List<string> ColumnsName = new List<string>();
            foreach (string jSA in jsonStringArray)
            {
                string[] jsonStringData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                foreach (string ColumnsNameData in jsonStringData)
                {
                    try
                    {
                        int idx = ColumnsNameData.IndexOf(":");
                        string ColumnsNameString = ColumnsNameData.Substring(0, idx - 1).Replace("\"", "");
                        if (!ColumnsName.Contains(ColumnsNameString))
                        {
                            ColumnsName.Add(ColumnsNameString);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Error Parsing Column Name : {0}", ColumnsNameData));
                    }
                }
                break;
            }
            foreach (string AddColumnName in ColumnsName)
            {
                dt.Columns.Add(AddColumnName);
            }
            foreach (string jSA in jsonStringArray)
            {
                string[] RowData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                DataRow nr = dt.NewRow();
                foreach (string rowData in RowData)
                {
                    try
                    {
                        int idx = rowData.IndexOf(":");
                        string RowColumns = rowData.Substring(0, idx - 1).Replace("\"", "");
                        string RowDataString = rowData.Substring(idx + 1).Replace("\"", "");
                        nr[RowColumns] = RowDataString;
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
                dt.Rows.Add(nr);
            }
            return dt;
        }
        protected void datagrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //Label LoginType = (Label)e.Row.FindControl("lblLoginType");
                //Label Email = (Label)e.Row.FindControl("lblEmail");
                //Label UserName = (Label)e.Row.FindControl("lblUserName");

                Label msgcount = (Label)e.Row.FindControl("lblcount");
                ImageButton imgButton = (ImageButton)e.Row.FindControl("Button1");
                int totalmsgs = Convert.ToInt32(msgcount.Text.ToString());

                foreach (TableCell cell in e.Row.Cells)
                {
                    if (totalmsgs > 0)
                    {
                        //msgcount.BackColor = Color.Green;
                        //msgcount.ForeColor = Color.White;

                        imgButton.ImageUrl = "~/Images/Chat.png";
                    }

                }
            }
        }

        protected void datagrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "OpenChat")
            {
                GridViewRow oItem = (GridViewRow)((ImageButton)e.CommandSource).NamingContainer;
                int RowIndex = oItem.RowIndex;

                Label atheletename = (datagrid.Rows[RowIndex].FindControl("lblAtheleteName") as Label);
                Label lblAtheleteId = (datagrid.Rows[RowIndex].FindControl("lblAtheleteId") as Label);
                Label lblDeviceId = (datagrid.Rows[RowIndex].FindControl("lblDeviceId") as Label);
                Label lblDeviceType = (datagrid.Rows[RowIndex].FindControl("lblDeviceType") as Label);

                Label LoginType = (datagrid.Rows[RowIndex].FindControl("lblLoginType") as Label);
                Label Email = (datagrid.Rows[RowIndex].FindControl("lblEmail") as Label);
                Label UserName = (datagrid.Rows[RowIndex].FindControl("lblUserName") as Label);
                Label lblSportId = (datagrid.Rows[RowIndex].FindControl("lblSportId") as Label);
                Label lblAdId = (datagrid.Rows[RowIndex].FindControl("lblAdId") as Label);

                //Session["lblSportId"] = Convert.ToInt16(lblSportId.Text);
                //Session["lblAdId"] = Convert.ToInt16( lblAdId.Text);
                //Session["LoginType"] =Convert.ToInt16( LoginType.Text);
               

                lblAdIdSend.Text = lblAdId.Text;
                lblSportIdSend.Text = lblSportId.Text;
                lblloginTypeSend.Text = LoginType.Text;

                string email = Email.Text;
                int LT = Convert.ToInt16(LoginType.Text);
                string UName = UserName.Text;

                int AtheleteId = Convert.ToInt32(lblAtheleteId.Text);
                Session["AtheleteNameSession"] = atheletename.Text;
                string name = atheletename.Text;
                lblAtheleteName.Text = name;
                lblAthIdSend.Text = AtheleteId.ToString();
                lblAthDevId.Text = lblDeviceId.Text;

                string emailUserName = string.Empty;
                if (LT == 1)
                {
                    emailUserName = email;
                    lblEmailUserNameSend.Text = email;
                    //Session["emailUserName"] = emailUserName;
                }
                else
                {
                    emailUserName = UName;
                    lblEmailUserNameSend.Text = UName;
                    //Session["emailUserName"] = emailUserName;
                }
                BindChatData(AtheleteId, emailUserName, LT, Convert.ToInt32(lblSportId.Text), Convert.ToInt32(lblAdId.Text));
            }
        }
        public void BindChatData(int AtheleteId, string emailUserName, int LoginType, int SportId, int adId)
        {

            // int SportId = 14;
            //DataTable TableData = ApiData.GetTableData(SportId);

            // string SportUrl = URLSelection.GetStagingUrl(SportId);
            string url = baseurl + "Ad/GetAthleteAdChatMessages";
            var client = new HttpClient();

            // string dummy="{"SenderId":1,"Max":100,"Id":0,"Message":"","Emailaddress":"puramamaheshbabu@gmail.com","Offset":0,"ReceiverType":0,"AdId":1,"ReceiverId":27,"LoginType":1,"Date":"1900-01-01","SenderType":1,"SportId":14}";


            int EndAdUserId = (int)System.Web.HttpContext.Current.Session["userId"];



            string inputString = "{\"SenderId\":\"" + AtheleteId + "\",\"ReceiverId\":\"" + EndAdUserId + "\",\"LoginType\":\"" + LoginType + "\",\"SportId\":\"" + SportId;
            inputString += "\",\"SenderType\":\"" + 0 + "\",\"ReceiverType\":\"" + 1 + "\",\"AdId\":\"" + adId + "\",\"Emailaddress\":\"" + emailUserName + "\",\"Date\":\"" + "2016-01-01";
            inputString += "\",\"Offset\":\"" + 0 + "\",\"Max\":\"" + 1000 + "\"}";

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpContent httpContent = new StringContent(inputString, Encoding.UTF8, "application/json");
            httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            var task = client.PostAsync(url, httpContent);

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            CustomResponse response = serializer.Deserialize<CustomResponse>(task.Result.Content.ReadAsStringAsync().Result);
            if (response.Response != null)
            {
                JsonSerializer ser = new JsonSerializer();
                string jsonresp = JsonConvert.SerializeObject(response);

                string subJson = jsonresp.Substring(23, jsonresp.Length - 64);

                DataTable ChatTable = JsonStringToDataTable(subJson);

                DataView dtview = new DataView(ChatTable);
                dtview.Sort = "Date ASC";
                DataTable dtsorted = dtview.ToTable();

                int count = ChatTable.Rows.Count;


                string msg = string.Empty;
                string senderType = string.Empty;
                string receiverType = string.Empty;
                for (int i = 0; i < count; i++)
                {
                    msg = dtsorted.Rows[i]["Message"].ToString();
                    senderType = dtsorted.Rows[i]["SenderType"].ToString();
                    receiverType = dtsorted.Rows[i]["ReceiverType"].ToString();

                    System.Web.UI.HtmlControls.HtmlTableRow row = new System.Web.UI.HtmlControls.HtmlTableRow();

                    if (senderType == "1")
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell cell = new System.Web.UI.HtmlControls.HtmlTableCell();
                        cell.Style.Add("width", "50%");
                        cell.Style.Add("text-align", "right");

                        Label lbl = new Label();
                        lbl.Text = msg;
                        cell.Controls.Add(lbl);
                        row.Cells.Add(cell);
                    }
                    else
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell cell = new System.Web.UI.HtmlControls.HtmlTableCell();
                        cell.Style.Add("width", "50%");
                        cell.Style.Add("text-align", "left");
                        cell.Style.Add("back-color", "#FF0000");

                        Label lbl = new Label();
                        lbl.Text = msg;
                        cell.Controls.Add(lbl);
                        row.Cells.Add(cell);
                    }
                    ChatData.Rows.Add(row);
                    divMessage.Controls.Add(ChatData);

                    // ChatWindow.Controls.Add(divMessage);
                }
            }
            else
            {

            }


            ChatWindow.Visible = true;
        }
        protected void btnSendMessage_Click(object sender, EventArgs e)
        {
            string ChatByFrontRush = txtPrivateMessage.Text;

            //int SportId = 14;
            //DataTable TableData = ApiData.GetTableData(SportId);
            string Date = DateTime.Now.ToString();
            int EndAdUserId = (int)System.Web.HttpContext.Current.Session["userId"];
            //int adId = (int)System.Web.HttpContext.Current.Session["lblAdId"];
            //int LoginType = (int)System.Web.HttpContext.Current.Session["LoginType"];
            //int SportId = (int)System.Web.HttpContext.Current.Session["lblSportId"];
            //string Email = (string)System.Web.HttpContext.Current.Session["emailUserName"];
            
            
            //string SportUrl = URLSelection.GetStagingUrl(SportId);
            string url = baseurl + "Ad/AddAthleteAdChat";
            var client = new HttpClient();

            string inputString = "{\"SenderId\":\"" + EndAdUserId + "\",\"ReceiverId\":\"" + lblAthIdSend.Text + "\",\"LoginType\":\"" + lblloginTypeSend.Text + "\",\"SportId\":\"" + lblSportIdSend.Text;
            inputString += "\",\"SenderType\":\"" + 1 + "\",\"ReceiverType\":\"" + 0 + "\",\"AdId\":\"" + lblAdIdSend.Text + "\",\"Message\":\"" + ChatByFrontRush + "\",\"Emailaddress\":\"" + lblEmailUserNameSend.Text + "\",\"Date\":\"" + Date;
            inputString += "\",\"Offset\":\"" + 0 + "\",\"Max\":\"" + 1000 + "\"}";

            //string inputString = "{\"SenderId\":\"" + 1 + "\",\"ReceiverId\":\"" + lblAthIdSend.Text;
            //inputString += "\",\"SenderType\":\"" + 1 + "\",\"ReceiverType\":\"" + 0 + "\",\"Date\":\"" + Date;
            //inputString += "\",\"Offset\":\"" + 0 + "\",\"Max\":\"" + 1000 + "\",\"Message\":\"" + ChatByFrontRush + "\"}";


            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpContent httpContent = new StringContent(inputString, Encoding.UTF8, "application/json");
            //httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            var task = client.PostAsync(url, httpContent);
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            CustomResponse response = serializer.Deserialize<CustomResponse>(task.Result.Content.ReadAsStringAsync().Result);

            txtPrivateMessage.Text = "";
            //BindChatData(Convert.ToInt16(lblAthIdSend.Text));

            BindChatData(Convert.ToInt16(lblAthIdSend.Text), lblEmailUserNameSend.Text,Convert.ToInt16(lblloginTypeSend.Text), Convert.ToInt32(lblSportIdSend.Text), Convert.ToInt32(lblAdIdSend.Text));
            
            ChatWindow.Visible = true;



            //string pushMessage = "A New Message From FrontRush!!!";
            //string Name = lblAthDevId.Text;
            //ClevertapPushNotification("Notification For FrontRush Msg to Athlete", pushMessage, Name);
        }

        public string CleverTapAccountId = ConfigurationManager.AppSettings["CleverTapAccountId"];
        public string CleverTapPassCode = ConfigurationManager.AppSettings["CleverTapPassCode"];

        public bool ClevertapPushNotification(string title, string pushMessage, string Name)
        {
            bool isPushMessageSend = false;
            try
            {
                string postString = "";
                string urlpath = "https://api.clevertap.com/1/send/push.json";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlpath);
                postString = "{ \"to\": {\"Identity\":[ \"" + Name + "\"  ], }," +
                                 "\"respect_frequency_caps\" :  false ," +
                                 "\"content\" : {\"title\":\"" + title + "\", \"body\":\"" + pushMessage + "\"}" +
                                 "}";

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.ContentLength = postString.Length;
                httpWebRequest.Headers.Add("X-CleverTap-Account-Id", CleverTapAccountId);
                httpWebRequest.Headers.Add("X-CleverTap-Passcode", CleverTapPassCode);
                httpWebRequest.Method = "POST";
                StreamWriter requestWriter = new StreamWriter(httpWebRequest.GetRequestStream());
                requestWriter.Write(postString);
                requestWriter.Close();
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    JObject jObjRes = JObject.Parse(responseText);
                    if (Convert.ToString(jObjRes).IndexOf("true") != -1)
                    {
                        isPushMessageSend = true;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isPushMessageSend;
        }
        protected void logout_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }

        protected void imagebutton1_Click(object sender, ImageClickEventArgs e)
        {
            ChatWindow.Visible = false;
        }

    }
}