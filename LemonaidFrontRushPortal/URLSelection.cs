﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LemonaidFrontRushPortal
{
    public class URLSelection
    {


        internal static string GetStagingUrl(int SportId)
        {

            switch (SportId)
            {
                case 1:
                    return "http://lemonaidstagingapi.azurewebsites.net/";

                case 2:
                    return "http://lemonaidrowingapistaging.azurewebsites.net/";
                    break;
                case 3:
                    return "http://lemonaiddivingapistaging.azurewebsites.net/";
                    break;
                case 4:
                    return "http://lemonaidtennisapistaging.azurewebsites.net/";
                    break;
                case 5:
                    return "http://lemonaidtrackapistaging.azurewebsites.net/";
                    break;
                case 6:
                    return "http://lemonaidlacrosseapistaging.azurewebsites.net/";
                    break;
                case 7:
                    return "http://lemonaidgolfapistaging.azurewebsites.net/";
                    break;
                case 8:
                    return "http://lemonaidwaterpoloapistaging.azurewebsites.net/";
                    break;
                case 9:
                    return "http://Lemonaidfootballapistaging.azurewebsites.net/";
                    break;
                case 10:
                    return "http://lemonaidbasketballapistaging.azurewebsites.net/";
                    break;
                case 11:
                    return "http://lemonaidbaseballapistaging.azurewebsites.net/";
                    break;
                case 12:
                    return "http://lemonaidsoccerapistaging.azurewebsites.net/";
                    break;
                case 13:
                    return "http://lemonaidsoftballapistaging.azurewebsites.net/";
                    break;
                case 14:
                    return "http://lemonaidwrestlingapistaging.azurewebsites.net/";
                    break;
                case 15:
                    return "http://lemonaidicehockeyapistaging.azurewebsites.net/";
                    break;
                case 16:
                    return "http://lemonaidvolleyballapistaging.azurewebsites.net/";
                    break;
                default:
                    return "0";
                    break;


            }

        }


        internal static string GetProductionUrl(int SportId)
        {

            switch (SportId)
            {
                case 1:
                    return "http://lemonaidbizapi.azurewebsites.net/";
                    break;
                case 2:
                    return "http://lemonaidrowingapi.azurewebsites.net/";
                    break;
                case 3:
                    return "http://lemonaiddivingapi.azurewebsites.net/";
                    break;
                case 4:
                    return "http://lemonaidtennisapi.azurewebsites.net/";
                    break;
                case 5:
                    return "http://lemonaidtrackapi.azurewebsites.net/";
                    break;
                case 6:
                    return "http://lemonaidlacrosseapi.azurewebsites.net/";
                    break;
                case 7:
                    return "http://lemonaidgolfapi.azurewebsites.net/";
                    break;
                case 8:
                    return "http://lemonaidwaterpoloapi.azurewebsites.net/";
                    break;
                case 9:
                    return "http://Lemonaidfootballapi.azurewebsites.net/";
                    break;
                case 10:
                    return "http://lemonaidbasketballapi.azurewebsites.net/";
                    break;
                case 11:
                    return "http://lemonaidbaseballapi.azurewebsites.net/";
                    break;
                case 12:
                    return "http://lemonaidsoccerapi.azurewebsites.net/";
                    break;
                case 13:
                    return "http://lemonaidsoftballapi.azurewebsites.net/";
                    break;
                case 14:
                    return "http://lemonaidwrestlingapi.azurewebsites.net/";
                    break;
                case 15:
                    return "http://lemonaidicehockeyapi.azurewebsites.net/";
                    break;
                case 16:
                    return "http://lemonaidvolleyballapi.azurewebsites.net/";
                    break;
                default:
                    return "0";
                    break;


            }

        }

        internal static int GetTeamType(int SportId)
        {
            switch (SportId)
            {
                case 1:
                    return 2;
                    break;
                case 2:
                    return 2;
                    break;
                case 3:
                    return 2;
                    break;
                case 4:
                    return 2;
                    break;
                case 5:
                    return 2;
                    break;
                case 6:
                    return 2;
                    break;
                case 7:
                    return 2;
                    break;
                case 8:
                    return 2;
                    break;
                case 9:
                    return 0;
                    break;
                case 10:
                    return 2;
                    break;
                case 11:
                    return 0;
                    break;
                case 12:
                    return 2;
                    break;
                case 13:
                    return 1;
                    break;
                case 14:
                    return 0;
                    break;
                case 15:
                    return 2;
                    break;
                case 16:
                    return 2;
                    break;
                default:
                    return 0;
                    break;


            }
        }
    }
}