﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Drawing;



namespace LemonaidFrontRushPortal
{
    public partial class Chats : System.Web.UI.Page
    {
        public static string baseurl = ConfigurationManager.AppSettings["BaseURl"].ToString();


        public static SqlConnection ConnectTodb()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["LemonaidDB"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("hi-IN");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("hi-IN");
                Deserialize_json_Object_and_Bind_GridView();
            }

            //if (Session["someSessionVal"].ToString() == "some value")
            //{
            //    ChatWindow.Visible = true;
            //}
            //else
            //{
            //    ChatWindow.Visible = false;
            //}
            //ChatWindow.Visible = false;
            //    BindChatData();
            ChatWindow.Visible = false;
        }

        public void Deserialize_json_Object_and_Bind_GridView()
        {

            //int SportId = (int)System.Web.HttpContext.Current.Session["SportID"];
            //int CoachID = (int)System.Web.HttpContext.Current.Session["userId"];


            int SportId = 14;

            string SportUrl = URLSelection.GetStagingUrl(SportId);
            string url = SportUrl + "Athlete2V4/GetAthletesForFrontRush?FrontRushId=1";

            WebRequest req = WebRequest.Create(url);
            req.ContentType = "application/json";
            WebResponse resp = req.GetResponse();
            Stream stream = resp.GetResponseStream();
            StreamReader re = new StreamReader(stream);
            String json = re.ReadToEnd();

            string subJson = json.Substring(23, json.Length - 64);


            DataTable dt = JsonStringToDataTable(subJson);
            if (dt.Rows.Count > 0)
            {
                datagrid.DataSource = dt;
                datagrid.DataBind();
            }
            else
            {
                lblMsg.Text = "No Athelete Chats Found!!!!";
                lblMsg.ForeColor = System.Drawing.Color.Red;
            }

        }

        public DataTable JsonStringToDataTable(string jsonString)
        {
            DataTable dt = new DataTable();
            string[] jsonStringArray = Regex.Split(jsonString.Replace("[", "").Replace("]", ""), "},{");
            List<string> ColumnsName = new List<string>();
            foreach (string jSA in jsonStringArray)
            {
                string[] jsonStringData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                foreach (string ColumnsNameData in jsonStringData)
                {
                    try
                    {
                        int idx = ColumnsNameData.IndexOf(":");
                        string ColumnsNameString = ColumnsNameData.Substring(0, idx - 1).Replace("\"", "");
                        if (!ColumnsName.Contains(ColumnsNameString))
                        {
                            ColumnsName.Add(ColumnsNameString);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Error Parsing Column Name : {0}", ColumnsNameData));
                    }
                }
                break;
            }
            foreach (string AddColumnName in ColumnsName)
            {
                dt.Columns.Add(AddColumnName);
            }
            foreach (string jSA in jsonStringArray)
            {
                string[] RowData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                DataRow nr = dt.NewRow();
                foreach (string rowData in RowData)
                {
                    try
                    {
                        int idx = rowData.IndexOf(":");
                        string RowColumns = rowData.Substring(0, idx - 1).Replace("\"", "");
                        string RowDataString = rowData.Substring(idx + 1).Replace("\"", "");
                        nr[RowColumns] = RowDataString;
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
                dt.Rows.Add(nr);
            }
            return dt;
        }

        protected void datagrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label msgcount = (Label)e.Row.FindControl("lblcount");
                int totalmsgs = Convert.ToInt32(msgcount.Text.ToString());

                foreach (TableCell cell in e.Row.Cells)
                {
                    if (totalmsgs > 0)
                    {
                        msgcount.BackColor = Color.Green;
                        msgcount.ForeColor = Color.White;
                    }

                }
            }
        }

        protected void datagrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "OpenChat")
            {
                GridViewRow oItem = (GridViewRow)((Button)e.CommandSource).NamingContainer;
                int RowIndex = oItem.RowIndex;

                Label atheletename = (datagrid.Rows[RowIndex].FindControl("lblAtheleteName") as Label);
                Label lblAtheleteId = (datagrid.Rows[RowIndex].FindControl("lblAtheleteId") as Label);
                Label lblDeviceId = (datagrid.Rows[RowIndex].FindControl("lblDeviceId") as Label);
                Label lblDeviceType = (datagrid.Rows[RowIndex].FindControl("lblDeviceType") as Label);

                int AtheleteId = Convert.ToInt32(lblAtheleteId.Text);
                Session["AtheleteNameSession"] = atheletename.Text;
                string name = atheletename.Text;
                lblAtheleteName.Text = name;
                lblAthIdSend.Text = AtheleteId.ToString();
                lblAthDevId.Text = lblDeviceId.Text;
                BindChatData(AtheleteId);
            }

        }

        public void BindChatData(int AtheleteId)
        {

            int SportId = 14;
            //DataTable TableData = ApiData.GetTableData(SportId);

            string SportUrl = URLSelection.GetStagingUrl(SportId);
            string url = SportUrl + "Athlete2V4/GetAthleteFrontRushChatMessages";
            var client = new HttpClient();
            string inputString = "{\"SenderId\":\"" + AtheleteId + "\",\"ReceiverId\":\"" + 1;
            inputString += "\",\"SenderType\":\"" + 0 + "\",\"ReceiverType\":\"" + 1 + "\",\"Date\":\"" + "1900-01-01";
            inputString += "\",\"Offset\":\"" + 0 + "\",\"Max\":\"" + 1000 + "\"}";

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpContent httpContent = new StringContent(inputString, Encoding.UTF8, "application/json");
            httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            var task = client.PostAsync(url, httpContent);

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            CustomResponse response = serializer.Deserialize<CustomResponse>(task.Result.Content.ReadAsStringAsync().Result);
            if (response.Response != null)
            {
                JsonSerializer ser = new JsonSerializer();
                string jsonresp = JsonConvert.SerializeObject(response);

                string subJson = jsonresp.Substring(23, jsonresp.Length - 64);

                DataTable ChatTable = JsonStringToDataTable(subJson);

                DataView dtview = new DataView(ChatTable);
                dtview.Sort = "Date ASC";
                DataTable dtsorted = dtview.ToTable();

                int count = ChatTable.Rows.Count;


                string msg = string.Empty;
                string senderType = string.Empty;
                string receiverType = string.Empty;
                for (int i = 0; i < count; i++)
                {
                    msg = dtsorted.Rows[i]["Message"].ToString();
                    senderType = dtsorted.Rows[i]["SenderType"].ToString();
                    receiverType = dtsorted.Rows[i]["ReceiverType"].ToString();

                    System.Web.UI.HtmlControls.HtmlTableRow row = new System.Web.UI.HtmlControls.HtmlTableRow();

                    if (senderType == "1")
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell cell = new System.Web.UI.HtmlControls.HtmlTableCell();
                        cell.Style.Add("width", "50%");
                        cell.Style.Add("text-align", "right");
                        Label lbl = new Label();
                        lbl.Text = msg;
                        cell.Controls.Add(lbl);
                        row.Cells.Add(cell);
                    }
                    else
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell cell = new System.Web.UI.HtmlControls.HtmlTableCell();
                        cell.Style.Add("width", "50%");
                        cell.Style.Add("text-align", "left");
                        Label lbl = new Label();
                        lbl.Text = msg;
                        cell.Controls.Add(lbl);
                        row.Cells.Add(cell);
                    }
                    ChatData.Rows.Add(row);
                    divMessage.Controls.Add(ChatData);

                    // ChatWindow.Controls.Add(divMessage);
                }
            }
            else
            {

            }


            ChatWindow.Visible = true;
        }
        public partial class ChatDTO
        {
            public int SenderId { get; set; }
            public int ReceiverId { get; set; }
            public int SenderType { get; set; }
            public int ReceiverType { get; set; }
            public DateTime Date { get; set; }
            public string Message { get; set; }
            public int Offset { get; set; }
            public int Max { get; set; }
            public int Count { get; set; }

        }

        protected void imagebutton1_Click(object sender, ImageClickEventArgs e)
        {
            ChatWindow.Visible = false;
        }

        protected void btnSendMessage_Click(object sender, EventArgs e)
        {
            string ChatByFrontRush = txtPrivateMessage.Text;

            int SportId = 14;
            //DataTable TableData = ApiData.GetTableData(SportId);
            string Date = DateTime.Now.ToString();

            string SportUrl = URLSelection.GetStagingUrl(SportId);
            string url = SportUrl + "Athlete2V4/AddAthleteFrontRushChat";
            var client = new HttpClient();


            string inputString = "{\"SenderId\":\"" + 1 + "\",\"ReceiverId\":\"" + lblAthIdSend.Text;
            inputString += "\",\"SenderType\":\"" + 1 + "\",\"ReceiverType\":\"" + 0 + "\",\"Date\":\"" + Date;
            inputString += "\",\"Offset\":\"" + 0 + "\",\"Max\":\"" + 1000 + "\",\"Message\":\"" + ChatByFrontRush + "\"}";


            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpContent httpContent = new StringContent(inputString, Encoding.UTF8, "application/json");
            //httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            var task = client.PostAsync(url, httpContent);
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            CustomResponse response = serializer.Deserialize<CustomResponse>(task.Result.Content.ReadAsStringAsync().Result);

            txtPrivateMessage.Text = "";
            BindChatData(Convert.ToInt16(lblAthIdSend.Text));

            ChatWindow.Visible = true;



          
            string pushMessage = "A New Message From FrontRush!!!";
            string Name = lblAthDevId.Text;
            ClevertapPushNotification("Notification For FrontRush Msg to Athlete", pushMessage, Name);
        }

        public string CleverTapAccountId = ConfigurationManager.AppSettings["CleverTapAccountId"];
        public string CleverTapPassCode = ConfigurationManager.AppSettings["CleverTapPassCode"];

        public bool ClevertapPushNotification(string title, string pushMessage, string Name)
        {
            bool isPushMessageSend = false;
            try
            {
                string postString = "";
                string urlpath = "https://api.clevertap.com/1/send/push.json";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlpath);
                postString = "{ \"to\": {\"Identity\":[ \"" + Name + "\"  ], }," +
                                 "\"respect_frequency_caps\" :  false ," +
                                 "\"content\" : {\"title\":\"" + title + "\", \"body\":\"" + pushMessage + "\"}" +
                                 "}";

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.ContentLength = postString.Length;
                httpWebRequest.Headers.Add("X-CleverTap-Account-Id", CleverTapAccountId);
                httpWebRequest.Headers.Add("X-CleverTap-Passcode", CleverTapPassCode);
                httpWebRequest.Method = "POST";
                StreamWriter requestWriter = new StreamWriter(httpWebRequest.GetRequestStream());
                requestWriter.Write(postString);
                requestWriter.Close();
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    JObject jObjRes = JObject.Parse(responseText);
                    if (Convert.ToString(jObjRes).IndexOf("true") != -1)
                    {
                        isPushMessageSend = true;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isPushMessageSend;
        }

        public static string GetJsonString(object data)
        {
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            return javaScriptSerializer.Serialize(data);
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }

       
    }
}
