﻿using System;
using System.Web.UI;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace LemonaidFrontRushPortal
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string Email = txtUserName.Text;
            string pwd = txtPWD.Text;

            string FrontRushUserName = "FRONTRUSH";

            if (Email != null && pwd != null)
            {
                CustomResponse res = Get("ad/GetEndUserId?Name=" + FrontRushUserName);
                string status = res.Status.ToString();
                string status1 = "Successful";
                if (status == status1)
                {
                    string Result = res.Response.ToString();
                    JObject o = JObject.Parse(Result);
                    int ID = (int)o["Id"];
                    Session["userId"] = ID;
                    string Name = (string)o["Name"];
                    string Emailaddress = (string)o["Emailaddress"];
                    string Password = (string)o["Password"];
                    if (Email == Emailaddress && pwd == Password)
                    {
                        Response.Redirect("FrontRushChats.aspx");
                    }
                    else
                    {
                        labelDisplayStatus.Text = "Please Enter Valid EmailAddress/Password!!!";
                    }
                }
                else
                {
                    labelDisplayStatus.Text = res.Message;
                    labelDisplayStatus.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                labelDisplayStatus.Text = "Please Enter Email And Password";
                labelDisplayStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        public class CustomResponse
        {
            public CustomResponseStatus Status;
            public Object Response;
            public string Message;
        }
        public enum CustomResponseStatus
        {
            Successful,
            UnSuccessful,
            Exception
        }
        public static CustomResponse Get(string apiurl)
        {

            try
            {
                CustomResponse res = new CustomResponse();
                string BaseUrl = "http://lemonaidwrapperstagingapi.azurewebsites.net/";
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(BaseUrl);
                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync(apiurl).Result;
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsAsync<CustomResponse>().Result;
                }
                else
                {
                }
                return res;
            }
            catch
            {
                throw;
            }
        }
    }
}