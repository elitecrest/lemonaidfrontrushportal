﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LemonaidFrontRushPortal
{
    public class DTO
    {
    }
    public class CustomResponse
    {
        public CustomResponseStatus Status;
        public Object Response;
        public string Message;
    }
    public enum CustomResponseStatus
    {
        Successful,
        UnSuccessful,
        Exception
    }

    public partial class EndUserDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Emailaddress { get; set; }
        public string Password { get; set; }
    }
    public partial class AthleteandAdDTO
    {
        public int AdId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ProfilePicURL { get; set; }
        public string WebsiteURL { get; set; }
        public int AdType { get; set; }
        public int EndAdUserId { get; set; }
        public string Emailaddress { get; set; }
        public string Username { get; set; }
        public int LoginType { get; set; }
        public int SportId { get; set; }
        public int ChatCount { get; set; }
        public int AthleteId { get; set; }
        public string AthleteName { get; set; }
        public string AthleteProfilePic { get; set; }
        public string DeviceId { get; set; }
        public string DeviceType { get; set; }

    }
}