﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LemonaidFrontRushPortal.Startup))]
namespace LemonaidFrontRushPortal
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
